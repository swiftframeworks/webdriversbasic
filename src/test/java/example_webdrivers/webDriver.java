package example_webdrivers;


import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class webDriver {

  private static WebDriver webdriver;
  Properties property = new Properties();

@Test
  public void invoke_phantomjs(){
    System.setProperty(getPropertyof("drivertypename_phantom"),getPropertyof("driverpath_phantom"));
      webdriver = new PhantomJSDriver();
      webdriver.get(getPropertyof("url"));
      webdriver.manage().timeouts().implicitlyWait(Long.parseLong(property.getProperty("time.seconds")), TimeUnit.SECONDS);
      webdriver.quit();

  }
    @Test
    public void invoke_chrome(){
        System.setProperty(getPropertyof("drivertypename_chrome"),getPropertyof("driverpath_chrome"));
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        webdriver = new ChromeDriver(options);
        webdriver.get(getPropertyof("url"));
        webdriver.manage().timeouts().implicitlyWait(Long.parseLong(property.getProperty("time.seconds")), TimeUnit.SECONDS);
        webdriver.quit();

    }
    @Test
    public void invoke_firefox(){
        System.setProperty(getPropertyof("drivertypename_gecko"),getPropertyof("driverpath_gecko"));
        webdriver = new FirefoxDriver();
        webdriver.get(getPropertyof("url"));
        webdriver.manage().timeouts().implicitlyWait(Long.parseLong(property.getProperty("time.seconds")), TimeUnit.SECONDS);
        webdriver.quit();

    }

    @Test
    public void invoke_seleniumGrid(){

       //please start selenium grid server first before running this test

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        caps.setPlatform(Platform.WINDOWS);

        try {
            // selenium grid
            String URL1 = "http://localhost:5555/wd/hub";
            WebDriver driver = new RemoteWebDriver(new URL(URL1),caps);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }



    }

  public String getPropertyof(String prop)
  {
                      try {

                          File file = new File(
                                  getClass().getClassLoader().getResource("settings.properties").getFile()
                          );
                          InputStream inputStream = new FileInputStream(file);
                          property.load(inputStream);

                      } catch (FileNotFoundException e) {
                          e.printStackTrace();
                      } catch (IOException e) {
                          e.printStackTrace();
                      }

                      return property.getProperty(prop);
  }

}//class
